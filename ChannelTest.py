import os
import sys
import time
import threading
import subprocess
import logging
from LifterAPI import *

cocomm_path = os.path.expanduser('~/CANopenLinux/cocomm/')
canopend_path = os.path.expanduser('~/CANopenLinux/')
os.environ['PATH'] = f'{cocomm_path}:{canopend_path}:{os.environ["PATH"]}'

def StartCan():
    os.system('ip link set can0 type can bitrate 500000')
    os.system('ip link set can0 up')

def StartCanOpend():
    os.system('rm "/tmp/CO_command_socket"')
    os.system('canopend can0 -i 55 -c "local-/tmp/CO_command_socket" &')

# Переменная для отслеживания состояния лифтера
toggle_flag = False

# Функция для переключения состояния лифтера по нажатию Enter
def toggle_lifter():
    global toggle_flag
    while True:
        input()  # Ожидание нажатия клавиши Enter
        toggle_flag = not toggle_flag  # Переключение флага  

# Основной цикл
def main_loop():
    global toggle_flag
    while True:
        if toggle_flag:
            Lifter_Up()
            print("Lifter: Up")
        else:
            Lifter_Down()
            print("Lifter: Down")
        time.sleep(2)

if __name__ == "__main__":
    StartCan()
    StartCanOpend()
    Lifter_SetEmergency()
    # Запуск потока для отслеживания нажатия Enter
    toggle_thread = threading.Thread(target=toggle_lifter)
    toggle_thread.daemon = True  # Поток завершится при завершении основного скрипта
    toggle_thread.start()
    
    # Запуск основного цикла
    main_loop()
