import os
import sys
import time
import threading
import subprocess
import logging
from LifterAPI import *

DELAY_TIME = 30  # Задержка между подьемом и опусканием
DELAY_TIME2 = 180 # Задержка ожидания
COUTER_WAIT = 3  # Соклько циклов подьема и опускания должно пройти, чтобы сработала задержка ожидания

cycle_counter = 0
previous_status = None
stop_flag = threading.Event()

# Вывод cчетчика комманд
def command_counter():
    command_counter = shell_exec('cocomm "13 r 0x6412 0 u32"')
    logging.info(f'COMMAND_COUNTER: {command_counter}')

# Настройка логирования
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S',
    handlers=[
        logging.FileHandler('LifterTest.log', mode='w'),
        logging.StreamHandler(sys.stdout)
    ]
)

# Поток 1 - Подьем/опускание
def lifter_control():
    global cycle_counter
    while not stop_flag.is_set():

        logging.info("UP")
        Lifter_Up()

        time.sleep(DELAY_TIME)

        logging.info("DOWN")
        Lifter_Down()

        time.sleep(DELAY_TIME)

        cycle_counter += 1

        if cycle_counter >= COUTER_WAIT:
            logging.info(f'Waiting for {DELAY_TIME2} seconds...')
            time.sleep(DELAY_TIME2)
            cycle_counter = 0

# Поток 2 - Вывод статуса и счетчика комманд
def status_monitor():
    global previous_status
    while not stop_flag.is_set():
        if Lifter_GetStatus() is None:
            logging.error('STATUS: EMPTY STRING')
            time.sleep(0.5)
            continue
        current_status = Lifter_GetStatus()
        if current_status != previous_status:
            logging.info(f'STATUS: {Lifter_GetTextStatus()}')
            previous_status = current_status
            command_counter()
        time.sleep(0.1)

#StartCan()
StartCanOpend()
Lifter_SetEmergency()

# Создание и запуск потоков
lifter_thread = threading.Thread(target=lifter_control)
status_thread = threading.Thread(target=status_monitor)

lifter_thread.start()
status_thread.start()

try:
    # Ожидаем завершения потоков в основном потоке
    lifter_thread.join()
    status_thread.join()
except KeyboardInterrupt:
    stop_flag.set()  # Останавливаем потоки при прерывании (например, Ctrl+C)
    lifter_thread.join()
    status_thread.join()
