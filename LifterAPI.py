import os
import sys
import time
import threading
import subprocess

def StartCan():
    os.system('ip link set can0 type can bitrate 500000')
    os.system('ip link set can0 up')

def StartCanOpend():
    cocomm_path = os.path.expanduser('./bin/')
    canopend_path = os.path.expanduser('./bin')
    os.environ['PATH'] = f'{cocomm_path}:{canopend_path}:{os.environ["PATH"]}'
    os.system('rm "/tmp/CO_command_socket"')
    os.system('canopend can0 -i 55 -c "local-/tmp/CO_command_socket" &')

# Выполнение команды оболочки с выводом в переменную
def shell_exec(command):
    try:
        # Запускаем команду через subprocess, перенаправляя вывод
        result = subprocess.run(command, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        # Преобразуем результат из байтов в строку и убираем лишние символы переноса строки
        output = result.stdout.decode('utf-8').strip()
        
        # Возвращаем результат
        return output
    except subprocess.CalledProcessError as e:
        # Если произошла ошибка, возвращаем текст ошибки
        #print("ERROR: shell_exec")
        print(f"Ошибка shell_exec: {e.stderr.decode('utf-8').strip()}\x1b[1A")
        #return f"Ошибка: {e.stderr.decode('utf-8').strip()}"
        return -1

def get_first_three_bits(n):
    # Маска для извлечения трёх младших битов (111 в двоичном формате)
    mask = 0b111
    # Извлечение трёх младших битов с помощью операции побитового И
    least_significant_bits = n & mask
    return least_significant_bits

def Lifter_SetEmergency():
    return shell_exec('cocomm "13 w 0x6405 0 u8 1"')

def Lifter_Up():
    return shell_exec('cocomm "13 w 0x6402 0 u8 1"')

def Lifter_Down():
    return shell_exec('cocomm "13 w 0x6402 0 u8 2"')

def Lifter_GetAllStatus():
    return shell_exec('cocomm "13 r 0x6404 0 u32"')

def Lifter_GetCurrent(channel):
    if channel == 1:
        return shell_exec('cocomm "13 r 0x640A 1 u32"')
    if channel == 2:
        return shell_exec('cocomm "13 r 0x640A 2 u32"')
    if channel == 3:
        return shell_exec('cocomm "13 r 0x640A 3 u32"')
    if channel == 4:
        return shell_exec('cocomm "13 r 0x640A 4 u32"')

def Lifter_GetCurrentFiltred(channel):
    if channel == 1:
        return shell_exec('cocomm "13 r 0x640A 5 u32"')
    if channel == 2:
        return shell_exec('cocomm "13 r 0x640A 6 u32"')
    if channel == 3:
        return shell_exec('cocomm "13 r 0x640A 7 u32"')
    if channel == 4:
        return shell_exec('cocomm "13 r 0x640A 8 u32"')

def Lifter_GetStatus():
    status = shell_exec('cocomm "13 r 0x6404 0 u32"')
    if status == '' or status == -1:
        return None  # Возвращаем None, если статус пустой
    try:
        status_int = int(status)
    except ValueError:
        print(f"Failed to convert status to int: '{status}'")
        return None  # Возвращаем None при ошибке преобразования
    three_status = get_first_three_bits(status_int)
    return three_status

def Lifter_GetTextStatus():
    status = Lifter_GetStatus()
    if status == 7:
        return 'INIT'
    if status == 1:
        return 'DOWN'
    if status == 2:
        return 'UP'
    if status == 3:
        return 'MOVE UP'
    if status == 4:
        return 'MOVE DOWN'
    if status == 5:
        return 'STOPPED'
    else:
        return status

def CurrentTest():
    print(f"1:{Lifter_GetCurrent(1)}")
    print(f"2:{Lifter_GetCurrent(2)}")
    print(f"3:{Lifter_GetCurrent(3)}")
    print(f"4:{Lifter_GetCurrent(4)}")
    print(f"5:{Lifter_GetCurrent(5)}")
    print(f"6:{Lifter_GetCurrent(6)}")
    print(f"7:{Lifter_GetCurrent(7)}")
    print(f"8:{Lifter_GetCurrent(8)}")
    print("\n")

#StartCan()
#StartCanOpend()
#Lifter_SetEmergency()
#Lifter_Up()
#Lifter_Down()
#CurrentTest()