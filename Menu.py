import curses
from LifterAPI import *

class CursesPrint:
    def __init__(self, stdscr):
        self.stdscr = stdscr
        self.y = 0
        self.x = 0

    def write(self, text):
        self.stdscr.addstr(self.y, self.x, text)
        self.stdscr.refresh()
        self.y += 1

    def flush(self):
        pass  # Не требуется для этого примера

class Menu:
    def __init__(self, title):
        self.title = title
        self.options = []
        self._message = ""  # Переименовать в _message для избежания конфликта

    def add_option(self, name, action):
        self.options.append(MenuOption(name, action))

    def set_message(self, msg):
        self._message = msg

    def display(self, stdscr):
        curses.curs_set(0)  # Скрыть курсор
        curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_WHITE)
        current_row = 0
        max_row = len(self.options) - 1

        while True:
            stdscr.clear()
            h, w = stdscr.getmaxyx()

            # Отобразить заголовок
            #stdscr.addstr(0, w // 2 - len(self.title) // 2, self.title)

            # Отобразить сообщение
            if self._message:
                stdscr.addstr(0, 0, self._message)

            # Отобразить опции меню
            for idx, option in enumerate(self.options):
                x = w // 2 - len(option.name) // 2
                y = h // 2 - len(self.options) // 2 + idx
                if idx == current_row:
                    stdscr.attron(curses.color_pair(1))
                    stdscr.addstr(y, x, option.name)
                    stdscr.attroff(curses.color_pair(1))
                else:
                    stdscr.addstr(y, x, option.name)

            stdscr.refresh()

            key = stdscr.getch()

            if key == curses.KEY_UP and current_row > 0:
                current_row -= 1
                self._message = ""  # Очистить сообщение при перемещении курсора
            elif key == curses.KEY_DOWN and current_row < max_row:
                current_row += 1
                self._message = ""  # Очистить сообщение при перемещении курсора
            elif key == curses.KEY_ENTER or key in [10, 13]:
                self.options[current_row].execute(stdscr)

class MenuOption:
    def __init__(self, name, action):
        self.name = name
        self.action = action

    def execute(self, stdscr):
        self.action(stdscr)

def action_LifterUp(stdscr):
    if Lifter_Up() == -1:
        menu.set_message("SDO Write error")

def action_LifterDown(stdscr):
    if Lifter_Down() == -1:
        menu.set_message("SDO Write error")

def action_LifterEmergency(stdscr):
    if Lifter_SetEmergency() == -1:
        menu.set_message("SDO Write error")

def action_LifterStatus(stdscr):
    status = Lifter_GetTextStatus()
    if status == None:
        menu.set_message("Error read status")
    else:
        menu.set_message(str(status))

def main():
    StartCan()
    StartCanOpend()

    global menu
    menu = Menu("Простое Меню")

    # Добавление опций в меню
    menu.add_option("Up", action_LifterUp)
    menu.add_option("Down", action_LifterDown)
    menu.add_option("Emergency", action_LifterEmergency)
    menu.add_option("Status", action_LifterStatus)

    curses.wrapper(menu.display)

if __name__ == "__main__":
    main()
